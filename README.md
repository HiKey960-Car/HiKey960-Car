# HiKey960-Car

# Parts required:
- Hikey960 SBC: https://www.96boards.org/product/hikey960/
- -- Other 96boards CE SBCs can also be used, however, we only maintain software for Hikey960.
- Automotive Mezzanine for 96boards
- Monitor, preferably with touchscreen.
- GPS receiver. USB (easiest) or 3.3v UART. It must be compatible with GPSd. Recommend a U-Blox 8.
- Standard automotive pigtail harness matching your car
- Radio antenna adapter, from your car's connector to SMA.
- Optional:
- -- Cameras,
- -- USB hub.
- -- other.

# Monitors
In general, you can use any HDMI monitor that you like, HOWEVER, the HiKey960 board is *not quite* able to reproduce the standard HDMI timings, so monitors that are extremely sensitive may not work with it. You probably want something with a USB touchscreen, although a 3.3v i2c can also be used if it is well supported by Linux. If you don't use a touchscreen, it is also possible to map steering wheel buttons to navigation controls, if you have an abundance of buttons.

# Features
Well, this is a general purpose computer that runs Android that is built into your car dashboard. It does *everything* that any other car radio can do, and *everything* that every other car radio PROMISES to do (even if they can't). It is an HFP client for phone calls, it runs mapping software, plays music, opens spreadsheets, whatever you can imagine.

# Limitations
There are two current software bugs that may be slightly annoying;
- No deep sleep. The unit will shut off entirely 30 seconds after you shut off the car, and reboots when you start back up. Boot time is about 20 seconds.
- If you run data over bluetooth, you will probably see occasional bluetooth crashes. The upstream DMA driver has a few bugs, but will hopefully be completed soon.

# How to build Android for this project
Read this: https://source.android.com/setup/devices<br>
Of course, changes are needed, in particular
* It is best to use a "known-good" manifest, there is one in this repository: "manifest-hikey960.xml".
* -- Instructions on using known good manifest to be found here: https://github.com/96boards/aosp-known-good-manifests
* Device tree from here: https://gitlab.com/HiKey960-Car/android_device_linaro_hikey
* Kernel from here: https://gitlab.com/HiKey960-Car/android_kernel_linaro_hikey
* Apply patches from the /patches/ path of this repository
* /external/bossa from here: https://gitlab.com/HiKey960-Car/bossa
* /external/gpsd from here: https://gitlab.com/HiKey960-Car/gpsd


- NOW BUILD ANDROID LIKE THIS:<br>
. build/envsetup.sh<br>
lunch hikey960_car-userdebug<br>
[HDMI_RES="{mode}"] m -j9<br>
* note: {mode} should be something along the lines of 1280x600@60 or 1024x600@70, etc.*<br>
* If you are having trouble getting your display working at its native resolution, read this: https://bugs.96boards.org/show_bug.cgi?id=827


# Steering wheel interface wiring
Parts needed:
- 2 1/4 watt 5% resistors.
<br>
The ADC's read voltage. You will be feeding it a voltage in the range of 0-3.3V. It will read 3.3V when the switches are all in the OFF position. When you push buttons, it will lower the voltage. You need to pick resistors that will allow the voltages to be spread out best over the range, but without getting too close to 3.3V where it won't read at all.<br>
<br>
First step is to measure the resistance of all the buttons on both banks. Write down the readings on the highest and the lowest.<br>
<br>
Pick a number that is right in the middle of them.<br>
<br>
My steering wheel has resistance in the range of 330-3200 ohms.<br>
Half way between them is ((3200 - 330) / 2) + 330 = 1765 ohms.<br>
<br>
That's what you're going for, as close as you can get to 1765 ohms, or whatever yours comes out to be.<br>
<br>
It does NOT have to be dead-on. Just get something reasonably close. For mine, 1.5k or 1.6k or 2.0k or 2.2k (very very common) would all be fine.<br>
<br>
Explanation:<br>
The formula is Vout = Vin * (R2 / (R1 + R2))<br>
We measure Vout<br>
Vin = 3.3V<br>
R2 is the resistor you are adding<br>
R1 is the button<br>
<br>
If R1 == R2, then Vout = 1.65V. Right in the middle of the range that we can read. So we pick R1 to be right in the middle in order to spread the readings out as much as possible.

# Dash Camera
Parts needed:<br>
*One or more USB video cameras that conform to USB Video Class.*<br><br>
My recommendation would be WIDE ANGLE lenses, like 170-180 degree "fisheye". Preferably the primary (front?) camera should have native h264 encoding capability. Additional cameras should support MJPEG.<br>
My personal preference is for OPEN CIRCUIT BOARD cameras, which can be screwed in to the front facing side of the rear-view mirror to avoid interfering with your view.<br>
Here is an example of a highly suitable camera; https://www.amazon.ca/ELP-Webcams-Surveillance-Customized-fisheye/dp/B0196BPZ1C<br>
<br>
If the camera has a built-in microphone, it can also support *audio capture*.<br>
<br>
Software:<br>
*ffmpeg*<br><br>
You will need to install a suitable STATIC or ANDROID build of ffmpeg, build it yourself if you feel like it, or use the officially supported static linux build available here;<br>
https://www.ffmpeg.org/download.html#build-linux<br>
Note: You will need the ARM64 build.<br>
Copy the file into /device/linaro/hikey/automotive/prebuilt/ and edit the two makefiles per the comments in the files to install.<br>
You may also like https://gitlab.com/HiKey960-Car/external_elpuvc if you happen to have one of their cameras with an XU to control the bitrate and other image settings. Put it in /external/elpuvc and add "elpuvc" to your PRODUCT_PACKAGES variable.

<br>
Camera setup:<br>
Plug in your Camera(s) to your USB hub.<br>
Note: It is important that your USB topology must remain static once the software setup is complete. The software will identify specific cameras based on the physical path of the devices.<br>
<br>
Once the cameras are plugged in (actually plug them in one at a time so that you know which one is which), look at the symlink targets in the path /sys/class/video4linux/<br>
Each entry will look something like this;<br>
video1 -> ../../devices/platform/soc/ff200000.hisi_usb/ff100000.dwc3/xhci-hcd.0.auto/usb1/1-1/1-1.2/1-1.2.1/1-1.2.1.4/1-1.2.1.4:1.0/video4linux/video1<br>
You need to select the final sub-path before /video4linux. In this case, you are looking for "/1-1.2.1.4:1.0/". You need that specific string, including the leading and trailing "/".<br>
<br>
The camera daemon receives its configuration via system properties. It recognizes up to 4 cameras, and 1 sound card.<br>
The properties are as follows;<br>
persist.dashcam.enabled<br>
persist.dashcam.X.path<br>
persist.dashcam.X.params<br>
persist.dashcam.X.sub<br>
persist.dashcam.X.extra<br>
persist.dashcam.audio.path<br>
Where X is one of front, rear, left, right. The only two that are mandatory are persist.dashcam.enabled and persist.dashcam.front.path<br>
<br>
To enable dashcam, run;<br>
setprop persist.dashcam.enabled 1<br>
<br>
To enable front camera, run;<br>
setprop persist.dashcam.front.path "/1-1.2.1.4:1.0/"<br>
(update value to match YOUR topology)<br>
<br>
If you need to modify the parameters of the front camera, run;<br>
setprop persist.dashcam.front.params "-input_format h264 -video_size 1280x720"<br>
<br>
Note that each camera could present more than one device. The suggested camera above for example, presents TWO. The first for RAW and MJPEG, the second for H264. If you need to select the second, use;<br>
setprop persist.dashcam.front.sub 1<br>
(the default/unset value is "0" for the first camera)<br>
<br>
Obtaining the audio path works exactly the same way, except you need to look in /sys/class/sound/, and you are looking only for links starting with "dsp".<br>
<br>
Once all parameters are set, reboot and it will begin recording.<br>
<br>
Extra controls: some cameras have an eXtension Unit (XU) that can control additional features of the camera. Some camera drivers expose these additional controls through the v4l2 interface automatically, others expose them manually, or only through a custom application. Those through the v4l2 interface may require v4l2-ctl to manipulate. ELP cameras provide manual exposure of the controls for v4l2, or access through a custom application available at https://gitlab.com/HiKey960-Car/external_elpuvc -- I will be adding an additional system property "persist.dashcam.X.extra to run a custom command prior to starting ffmpeg. Any instance of the character '$' in the propval will be replaced by the device path of the camera, i.e. "/dev/video0"<br>
<br>
Example 2-camera setup (front 1920x1080 h264, rear 1280x720 mjpeg):<br>
[persist.dashcam.audio.path]: [/1-1.1.2:1.3/]<br>
[persist.dashcam.enabled]: [1]<br>
[persist.dashcam.front.extra]: [/system/bin/elpuvc --xuset-br 4000000 $]<br>
[persist.dashcam.front.params]: [-input_format h264 -video_size 1920x1080]<br>
[persist.dashcam.front.path]: [/1-1.1.2:1.0/]<br>
[persist.dashcam.front.sub]: [1]<br>
[persist.dashcam.rear.extra]: [/system/bin/elpuvc --xuset-mjb 5000000 $]<br>
[persist.dashcam.rear.params]: [-input_format mjpeg -video_size 1280x720 -framerate 15]<br>
[persist.dashcam.rear.path]: [/1-1.1.1.3:1.0/]<br>
[persist.dashcam.rear.sub]: [0]<br>

# Microcontroller
The Mezzanine board is equipped with a SAMD21 microcontroller, used for various 3.3v interfacing, power control, steering wheel interface, backlight control, and fan control. It can be programmed with 'bossac' through the application "CarSettings". Firmware binaries can be generated using arduino ide configured as per https://gitlab.com/HiKey960-Car/mcu_firmware<br>

# The Mezzanine
This is a mezzanine board for 96boards "CE" standard footprint. It should technically work with any 96boards CE compliant SBC, with one caveat: for full functionality, it requires a FULL 4-wire I2S interface. The I2S input pin is, according to 96boards CE specifications, OPTIONAL. I am aware of one board that DOES NOT implement the input pin: Dragonboard 410C. This missing input pin will most obviously impact the ability to use the microphone interface on the Mezzanine, which means that for HFP to work, you would have to find another means of connecting a microphone. Note that this missing pin WILL NOT impact the functionality of the AMFM radio, since its audio stream will be mixed into the output stream on the mezzanine board itself -- it does not have to be processed by the SBC.<br>
<br>
**Specifications:**
- 123 mm wide X 78 mm high
- Standardized 96boards CE interface to SBC
- Sound system consisting of
- --   Texas Instruments PCM1865 ADC
- --   Texas Instruments PCM5142 DAC (x2)
- --   STMicroelectronics STPA003 Amplifier
- --   NXP TEF6686 Broadcast Radio Receiver
- NXP PCF85063A Real Time Clock, with 1.5F supercapacitor backup
- AT SAMD21E microcontroller
- --   Steering wheel interface
- --   VN750PS-E switch for 14V, including self-power control
- --   TPS22918 switch for 5V
- Input signals received by both SBC and Microcontroller
- --   ACC
- --   Headlight
- --   Reverse
- ** input signals are optically isolated

**Contact me for additional details and to obtain a board.**
 